﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hualqui_aux.Controllers
{
    public class InfraccionController : Controller
    {
        // GET: Infraccion
        public ActionResult Index()
        {
            return View();
        }

        // GET: Infraccion/Details/5
        public ActionResult listado()
        {
            return View();
        }

        // GET: Infraccion/Create
        public ActionResult crear()
        {
            return View();
        }

        // POST: Infraccion/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Infraccion/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Infraccion/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Infraccion/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Infraccion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
